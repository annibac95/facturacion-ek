<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Articulo;

class ArticulosController extends Controller
{
    //
    public function _construc(){
        $this->middleware('auth');
    }
    
    public function index(){
        $articulos= Articulo::all();
       
        return view('admin.articulos.index', (compact('articulos')));
    }
    
    public function store(Request $request){
        $newArticulo = new Articulo();
        $newArticulo->nombre= $request->nombre;
        $newArticulo->precio= $request->precio;
        $newArticulo->cantidad= $request->cantidad;
        $newArticulo->save();
        
        return redirect()->back();
    }

    public function update(Request $request, $articuloId){
        $articulo = Articulo::find($articuloId);
        $articulo->nombre= $request->nombre;
        $articulo->precio= $request->precio;
        $articulo->cantidad= $request->cantidad;
        $articulo->save();
        
        return redirect()->back();
    }

    public function delete(Request $request, $articuloId){
        $articulo = Articulo::find($articuloId);
        $articulo->delete();
        
        return redirect()->back();
    }
}
