<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;
    public function cliente(){
        return $this->belongsTo('App\Models\Cliente');
    }

    public function articulos(){
        return $this->belongsToMany('App\Models\Articulo','factura_articulo');
    }
}