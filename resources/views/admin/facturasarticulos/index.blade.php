<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Detalle Factura</title>
  </head>
  <body>

   <div class="container mt-4">
       <div class="row justify-content-center">
            <div class="col-auto">
                <h3>Factura número: <span class="badge bg-secondary">{{$factura->id}}</span> tiene el siguiente detalle</h3>
                <table class="table table-striped table-hover">
                    <thead class="bg-primary text-white">
                        <th>Articulo</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        
                    </thead>
                    <tbody>
                        @foreach ($factura->articulos as $detalle)
                        <tr>
                            <td>{{$detalle->nombre}}</td>
                            <td>{{$detalle->precio}}</td>
                            <td>{{$detalle->cantidad}}</td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
       </div>
   </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>