<!-- modal UPDATE cliente-->
                    
<div class="modal fade" id="modal-update-clientes-{{$cliente->id}}">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Actualizar Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.clientes.update', $cliente->id)}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">
                    <div class='form-group'>
                        <label for="nombre">Nombre</label>
                        <input type="char" name="nombre" class="form-control" id="nombre" value="{{ $cliente->nombre}}">
                        <label for="ruc">Ruc</label>
                        <input type="char" name="ruc" class="form-control" id="ruc" value="{{ $cliente->ruc}}">
                        <label for="telefono">Teléfono</label>
                        <input type="char" name="telefono" class="form-control" id="telefono" value="{{ $cliente->telefono}}">
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->