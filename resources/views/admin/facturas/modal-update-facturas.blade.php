<!-- modal UPDATE facturas-->
                    
<div class="modal fade" id="modal-update-facturas-{{$factura->id}}">
    <div class="modal-dialog">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Actualizar Factura</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            
                <form action="{{route('admin.facturas.update', $factura->id)}}" method="POST">
                    {{csrf_field()}}
                <div class="modal-body">

                <div class='form-group'>
                        <label for="cliente_id">Cliente</label>
                        <select name="cliente_id" class="form-control" id="cliente_id">
                            <option value="{{$factura->cliente_id}}"> {{$factura->cliente->nombre}} </option>
                            @foreach($clientes as $cliente)
                            <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
                            @endforeach
                        </select> 
                     </div>
                   
                    <div class='form-group'>
                    <label for="fechahora">Fecha y Hora</label>
                        <input type="date" name="fechahora" class="form-control" id="fechahora" value="{{$factura->fechahora}}">
                        
                    </div>
                    <div class='form-group'>
                    <label for="estado">Estado</label>
                        <input type="char" name="estado" class="form-control" id="estado" value="{{ $factura->estado}}">
                    
                    </div>
                     
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Guardar</button>
                </div>
                </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->